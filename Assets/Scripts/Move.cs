﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Rigidbody2D rigidBody2D = GetComponent<Rigidbody2D>();
        float angle = gameObject.transform.eulerAngles.z*Mathf.Deg2Rad;
        Vector2 forward = new Vector2(Mathf.Cos(angle + 0.5f * Mathf.PI), Mathf.Sin(angle + 0.5f * Mathf.PI));
        Vector2 backward = -forward;
        Vector2 right = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
        Vector2 left = -right;

        Vector3 playerPosition = gameObject.transform.position;

        if (Input.GetKey(KeyCode.W)) rigidBody2D.AddForce(15*forward);
        if (Input.GetKey(KeyCode.S)) rigidBody2D.AddForce(15*backward);
        if (Input.GetKey(KeyCode.A)) rigidBody2D.AddForce(15*left);
        if (Input.GetKey(KeyCode.D)) rigidBody2D.AddForce(15*right);
        if (Input.GetKey(KeyCode.Q)) rigidBody2D.AddTorque( 1);
        if (Input.GetKey(KeyCode.E)) rigidBody2D.AddTorque(-1);
    }
}
