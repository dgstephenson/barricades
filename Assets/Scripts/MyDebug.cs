﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyDebug : MonoBehaviour {

    public Material material;
    public List<Vector2> lineStartPoints;
    public List<Vector2> lineEndPoints;
    public List<Color> lineColors;

    // Use this for initialization
    void Start()
    {
        Shader shader = Shader.Find("Hidden/Internal-Colored");
        material = new Material(shader);
        lineStartPoints = new List<Vector2>();
        lineEndPoints = new List<Vector2>();
        lineColors = new List<Color>();
    }

    public void DrawLine(Vector3 start, Vector3 end, Color color)
    {
        lineStartPoints.Add(start);
        lineEndPoints.Add(end);
        lineColors.Add(color);
    }

    void OnPostRender()
    {
        material.SetPass(0);
        Debug.Log("OnPostRender");
        Debug.Log("lineStartPoints.ToArray().Length = "+ lineStartPoints.ToArray().Length);
        for (int i = 0; i < lineStartPoints.ToArray().Length; i++)
        {
            GL.Begin(GL.LINES);
            GL.Color(lineColors[i]);
            GL.Vertex(lineStartPoints[i]);
            GL.Vertex(lineEndPoints[i]);
            GL.End();
        } 
        lineStartPoints.Clear();
        lineEndPoints.Clear();
        lineColors.Clear();
    }

}
