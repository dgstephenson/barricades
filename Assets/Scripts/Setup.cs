﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

public class Setup : MonoBehaviour
{
    public List<Vector3> corners;
    public List<Vector3[]> edges;
    public List<Vector3[]> viewEdges;
    public Vector2 halfView;
    public Sprite square;
    public GameObject mainCameraObject;
    public GameObject player;
    public Camera mainCamera;
    public GameObject lightMask;
    public List<Vector2> vertList;
    public List<ushort> triList;
    public MyDebug myDebug;

    void Start()
    {

        corners = new List<Vector3>();
        edges = new List<Vector3[]>();
        halfView = new Vector2(30, 20);

        square = Sprite.Create(Texture2D.whiteTexture, new Rect(0, 0, 1, 1), new Vector2(0.5f, 0.5f), 1);
        square.name = "Square";

        player = makePlayer();
        Vector2 viewPoint1 = player.transform.position + new Vector3( halfView.x,  halfView.y);
        Vector2 viewPoint2 = player.transform.position + new Vector3( halfView.x, -halfView.y);
        Vector2 viewPoint3 = player.transform.position + new Vector3(-halfView.x, -halfView.y);
        Vector2 viewPoint4 = player.transform.position + new Vector3(-halfView.x,  halfView.y);
        corners.Add(viewPoint1);
        corners.Add(viewPoint2);
        corners.Add(viewPoint3);
        corners.Add(viewPoint4);

        mainCamera = makeCamera();
        myDebug = mainCamera.gameObject.AddComponent<MyDebug>();

        makeWall(new Vector3(5, 0), 10, 5, 0);
        makeWall(new Vector3(0, 20), 3, 15, 0);
        makeWall(new Vector3(-20, -5), 10, 3, 20);

        GameObject curtain = makeGameObject(name: "Curtain", scale: new Vector3(500, 500, 1));
        Sprite curtainSprite = Sprite.Create(Texture2D.whiteTexture, new Rect(0, 0, 1, 1), new Vector2(0.5f, 0.5f), 1);
        SpriteRenderer curtainSpriteRenderer = curtain.AddComponent<SpriteRenderer>();
        curtainSpriteRenderer.sprite = curtainSprite;
        curtainSpriteRenderer.color = Color.black;
        curtainSpriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        curtainSpriteRenderer.sortingLayerName = "Curtain";
        //curtain.SetActive(false);

        lightMask = makeGameObject(name: "Light Mask", parent: player);
        lightMask.transform.localPosition = new Vector3(0, 4, 0);
        lightMask.transform.localScale = new Vector3(halfView.x, halfView.y, 1);
        Vector2 p1 = new Vector2(0f, 0f);
        Vector2 p2 = new Vector2(0f, 1f);
        Vector2 p3 = new Vector2(1f, 1f);
        Vector2 p4 = new Vector2(1f, 0f);
        Vector2[] verts = new Vector2[] { p1, p2, p3, p4 };
        Sprite maskSprite = Sprite.Create(Texture2D.whiteTexture,new Rect(0, 0, 1, 1),new Vector2(0.5f, 0.5f),1);
        maskSprite.OverrideGeometry(verts, new ushort[] { 0, 1, 3, 1, 2, 3 });
        SpriteMask curtainSpriteMask = lightMask.AddComponent<SpriteMask>();
        curtainSpriteMask.sprite = maskSprite;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 playerPosition = player.transform.position;
        SetupViewEdges();
        Vector2[] pointArray = GetLightPoints();
        SetupLightMask(pointArray);
    }

    Vector2[] GetLightPoints()
    {
        Vector2 playerPosition = player.transform.position;
        List<Vector2> points = new List<Vector2>();
        foreach (Vector3[] viewEdge in viewEdges)
        {
            myDebug.DrawLine(viewEdge[0], viewEdge[1], Color.cyan);
            foreach (Vector3[] edge in edges)
            {
                Vector2? intersection = getIntersection(edge[0], edge[1], viewEdge[0], viewEdge[1]);
                if (intersection != null) if (isVisible(intersection.Value)) points.Add(intersection.Value);
            }
        }
        foreach (Vector3 corner in corners) if (isVisible(corner)) points.Add(corner);
        foreach (Vector2 point in points) myDebug.DrawLine(playerPosition, getEndPoint(point), Color.cyan);
        var orderedPointList = 
            from point in points
            orderby Vector2.SignedAngle(Vector2.up, (Vector2)point - playerPosition)
            select point;
        return orderedPointList.ToArray();
    }

    void SetupViewEdges()
    {
        foreach (Vector3[] edge in edges)
        {
            myDebug.DrawLine(edge[0], edge[1], Color.blue);
        }
        Vector2 viewPoint0 = lightMask.transform.TransformPoint(Vector2.right / 3 + Vector2.up / 3);
        Vector2 viewPoint1 = lightMask.transform.TransformPoint(Vector2.right / 3 + -Vector2.up / 3);
        Vector2 viewPoint2 = lightMask.transform.TransformPoint(-Vector2.right / 3 + -Vector2.up / 3);
        Vector2 viewPoint3 = lightMask.transform.TransformPoint(-Vector2.right / 3 + Vector2.up / 3);
        corners[0] = viewPoint0;
        corners[1] = viewPoint1;
        corners[2] = viewPoint2;
        corners[3] = viewPoint3;
        viewEdges = new List<Vector3[]>(){
            new Vector3[] { viewPoint0, viewPoint1 },
            new Vector3[] { viewPoint1, viewPoint2 },
            new Vector3[] { viewPoint2, viewPoint3 },
            new Vector3[] { viewPoint3, viewPoint0 },
        };
    }

    void SetupLightMask(Vector2[] pointArray)
    {
        Vector2 playerPosition = player.transform.position;
        vertList = new List<Vector2>();
        vertList.Add(getLightPoint(playerPosition));
        triList = new List<ushort>();
        for (int i = 0; i < pointArray.Length; i++)
        {
            int j = i + 1;
            if (j == pointArray.Length) j = 0;
            Vector2 point = pointArray[i];
            Vector2 endPoint = getEndPoint(point);
            Vector2 nextPoint = pointArray[j];
            Vector2 nextEndPoint = getEndPoint(nextPoint);
            if (isVisibleSegment(endPoint, nextEndPoint))
            {
                myDebug.DrawLine(endPoint, nextEndPoint, Color.red);
                vertList.Add(getLightPoint(endPoint));
                vertList.Add(getLightPoint(nextEndPoint));
            }
            else if (isVisibleSegment(endPoint, nextPoint))
            {
                myDebug.DrawLine(endPoint, nextPoint, Color.red);
                vertList.Add(getLightPoint(endPoint));
                vertList.Add(getLightPoint(nextPoint));
            }
            else if (isVisibleSegment(point, nextEndPoint))
            {
                myDebug.DrawLine(point, nextEndPoint, Color.red);
                vertList.Add(getLightPoint(point));
                vertList.Add(getLightPoint(nextEndPoint));
            }
            else
            {
                myDebug.DrawLine(point, nextPoint, Color.red);
                vertList.Add(getLightPoint(point));
                vertList.Add(getLightPoint(nextPoint));
            }
            myDebug.DrawLine(point, endPoint, Color.red);
            int length = vertList.ToArray().Length;
            triList.Add(0);
            triList.Add((ushort)(length - 1));
            triList.Add((ushort)(length - 2));
        }
        lightMask.GetComponent<SpriteMask>().sprite.OverrideGeometry(vertList.ToArray(), triList.ToArray());
    }

    Vector2 getLightPoint(Vector3 worldPoint)
    {
        Vector2 localPoint = lightMask.transform.InverseTransformPoint(worldPoint) + new Vector3(1, 1, 0) * 0.5f;
        localPoint.x = Mathf.Clamp01(localPoint.x);
        localPoint.y = Mathf.Clamp01(localPoint.y);
        return (localPoint);
    }

    bool inRange(Vector2 point)
    {
        Vector2 localPoint = lightMask.transform.InverseTransformPoint(point);
        float min = -1f / 3f - 0.001f;
        float max = 1f / 3f + 0.001f;
        if (min <= localPoint.x && localPoint.x <= max && min <= localPoint.y && localPoint.y <= max)
        {
            return true;
        }
        else return false;
    }

    bool isVisibleSegment(Vector2 a, Vector2 b)
    {
        List<Vector2> points = new List<Vector2>();
        points.Add(0.99f * a + 0.01f * b);
        points.Add(0.30f * a + 0.70f * b);
        points.Add(0.50f * a + 0.50f * b);
        points.Add(0.70f * a + 0.30f * b);
        points.Add(0.01f * a + 0.99f * b);
        return points.TrueForAll(isVisible);

    }

    bool isVisible(Vector2 point)
    {
        if (inRange(point))
        {
            Vector2 playerPosition = (Vector2)player.transform.position;
            Vector2 relativePoint = point - playerPosition;
            float distance = relativePoint.magnitude;
            RaycastHit2D hit = Physics2D.Raycast(playerPosition, relativePoint);
            Vector2 relativeHitPoint = hit.point - playerPosition;
            float hitDistance = relativeHitPoint.magnitude;
            if (hit.distance == 0 || hitDistance + 0.01 > distance) return true;
            else return false;
        }
        else return false;
    }

    bool isObstruction(Vector2 point)
    {
        Vector2 playerPosition = (Vector2)player.transform.position;
        Vector2 relativePoint = point - playerPosition;
        float distance = relativePoint.magnitude;
        Vector2 direction = relativePoint / distance;
        Vector2 behindPoint = point + 0.1f * direction;
        Collider2D behindCollider = Physics2D.OverlapPoint(behindPoint);
        return (behindCollider != null);
    }

    Vector2 getEndPoint(Vector2 point)
    {
        if (isObstruction(point)) return point;
        else
        {
            Vector2 playerPosition = (Vector2)player.transform.position;
            Vector2 relativePosition = point - playerPosition;
            float distance = relativePosition.magnitude;
            Vector2 direction = relativePosition / distance;
            RaycastHit2D endHit = Physics2D.Raycast(point+0.1f*direction, direction);
            Vector2 endPoint = point;
            if (inRange(endHit.point) && endHit.point != Vector2.zero) endPoint = endHit.point;
            else
            {
                endPoint = point;
                Vector2 a = point + 0.1f * direction;
                Vector2 b = point + 1000 * direction;
                foreach(Vector3[] edge in viewEdges)
                {
                    Vector2? intersection = getIntersection(a, b, (Vector2)edge[0], (Vector2)edge[1]);
                    if (intersection.HasValue) endPoint = intersection.Value;
                }
            }
            return endPoint;
        }
    }

    GameObject makeWall(Vector3 position, float halfHeight, float halfWidth, float rotation)
    {
        GameObject wall = makeGameObject(name: "Wall", position: position);
        SpriteRenderer wallSpriteRenderer = wall.AddComponent<SpriteRenderer>();
        wallSpriteRenderer.color = Color.green;
        wallSpriteRenderer.sortingLayerName = "Walls";
        wallSpriteRenderer.sprite = square;
        PolygonCollider2D wallCollider = wall.AddComponent<PolygonCollider2D>();
        wallCollider.CreatePrimitive(4);
        Vector2 wallPoint0 = new Vector2(-0.5f, -0.5f);
        Vector2 wallPoint1 = new Vector2(-0.5f, 0.5f);
        Vector2 wallPoint2 = new Vector2(0.5f, 0.5f);
        Vector2 wallPoint3 = new Vector2(0.5f, -0.5f);
        wallCollider.points = new Vector2[] { wallPoint0, wallPoint1, wallPoint2, wallPoint3 };
        wall.transform.localScale = new Vector3(2 * halfWidth, 2*halfHeight, 1);
        GameObject corner1 = makeGameObject(name: "Corner", position: position + new Vector3(  halfWidth,  halfHeight), parent: wall);
        GameObject corner2 = makeGameObject(name: "Corner", position: position + new Vector3(  halfWidth, -halfHeight), parent: wall);
        GameObject corner3 = makeGameObject(name: "Corner", position: position + new Vector3( -halfWidth, -halfHeight), parent: wall);
        GameObject corner4 = makeGameObject(name: "Corner", position: position + new Vector3( -halfWidth,  halfHeight), parent: wall);
        wall.transform.eulerAngles = new Vector3(0, 0, rotation);
        corners.Add(corner1.transform.position);
        corners.Add(corner2.transform.position);
        corners.Add(corner3.transform.position);
        corners.Add(corner4.transform.position);
        edges.Add(new Vector3[] { corner1.transform.position, corner2.transform.position });
        edges.Add(new Vector3[] { corner2.transform.position, corner3.transform.position });
        edges.Add(new Vector3[] { corner3.transform.position, corner4.transform.position });
        edges.Add(new Vector3[] { corner4.transform.position, corner1.transform.position });
        return wall;
    }

    GameObject makePlayer()
    {
        int count = 5;
        Vector2[] vertices = new Vector2[count];
        for (int i = 0; i < count; i++)
        {
            float angle = (90 + i * 360.0f / count) * Mathf.PI / 180;
            vertices[i] = 0.5f * Vector2.one + 0.5f * Mathf.Cos(angle) * Vector2.right + 0.5f * Mathf.Sin(angle) * Vector2.up;
        }
        Triangulator triangulator = new Triangulator(vertices);
        ushort[] indices = triangulator.Triangulate().Select(x => (ushort)x).ToArray();
        Sprite polygon = Sprite.Create(Texture2D.whiteTexture, new Rect(0, 0, 1, 1), new Vector2(0.5f, 0.5f), 1);
        polygon.name = "Polygon";
        polygon.OverrideGeometry(vertices, indices);
        GameObject player = makeGameObject(name: "Player", position: 2 * Vector2.left, scale: 3 * Vector3.one);
        SpriteRenderer playerSpriteRenderer = player.AddComponent<SpriteRenderer>();
        playerSpriteRenderer.color = Color.blue;
        playerSpriteRenderer.sortingLayerName = "People";
        playerSpriteRenderer.sprite = polygon;
        player.AddComponent<PolygonCollider2D>();
        player.AddComponent<Rigidbody2D>();
        player.GetComponent<Rigidbody2D>().drag = 5;
        player.GetComponent<Rigidbody2D>().angularDrag = 2;
        player.AddComponent<Move>();
        return player;
    }

    Camera makeCamera()
    {
        mainCameraObject = makeGameObject(name: "MainCameraObject");
        Camera mainCamera = mainCameraObject.AddComponent<Camera>();
        mainCamera.orthographic = true;
        mainCamera.orthographicSize = 40f;
        mainCamera.backgroundColor = Color.white;
        mainCameraObject.transform.position = 3 * Vector3.back;
        mainCameraObject.transform.parent = player.transform;
        return mainCamera;
    }

    public static GameObject addMarker(Vector3 position)
    {
        GameObject marker = makeGameObject(name: "Marker", position: position, scale: new Vector3(0.1f, 0.1f, 1f));
        Sprite markerSprite = Sprite.Create(
            Texture2D.whiteTexture,
            new Rect(0, 0, 1, 1),
            new Vector2(0.5f, 0.5f),
            0.1f
        );
        SpriteRenderer markerSpriteRenderer = marker.AddComponent<SpriteRenderer>();
        markerSpriteRenderer.sprite = markerSprite;
        markerSpriteRenderer.color = Color.red;
        markerSpriteRenderer.sortingLayerName = "Markers";
        //markerSpriteRenderer.enabled = false;
        return marker;
    }

    public static GameObject makeGameObject(
        string name,
        Vector3? position = null,
        Vector3? scale = null,
        GameObject parent = null
    )
    {

        GameObject gameObject = new GameObject();
        gameObject.name = name;
        gameObject.transform.position = position ?? Vector3.zero;

        if (parent)
        {
            gameObject.transform.parent = parent.transform;
        }

        gameObject.transform.localScale = scale ?? Vector3.one;

        return gameObject;
    }

    public Vector2? getIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
    {
        Vector2 intersection = Vector2.zero;
        var d = (p2.x - p1.x) * (p4.y - p3.y) - (p2.y - p1.y) * (p4.x - p3.x);
        if (d == 0.0f)
        {
            return null;
        }
        var u = ((p3.x - p1.x) * (p4.y - p3.y) - (p3.y - p1.y) * (p4.x - p3.x)) / d;
        var v = ((p3.x - p1.x) * (p2.y - p1.y) - (p3.y - p1.y) * (p2.x - p1.x)) / d;
        if (u < 0.0f || u > 1.0f || v < 0.0f || v > 1.0f)
        {
            return null;
        }
        intersection.x = p1.x + u * (p2.x - p1.x);
        intersection.y = p1.y + u * (p2.y - p1.y);
        return intersection;
    }

}
